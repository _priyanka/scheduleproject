//
//  ViewController.swift
//  ProjectSchedule
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

class ViewController: UIViewController {
  
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var dayOne: UILabel!
    @IBOutlet var dayTwo: UILabel!
    @IBOutlet var dayThree: UILabel!
    @IBOutlet var dayFour: UILabel!
    @IBOutlet var dayFive: UILabel!
    @IBOutlet var daySix: UILabel!
    @IBOutlet var daySeven: UILabel!
    @IBOutlet var imgOne: UIImageView!
    @IBOutlet var imgTwo: UIImageView!
    @IBOutlet var imgThree: UIImageView!
    @IBOutlet var imgFour: UIImageView!
    @IBOutlet var imgFive: UIImageView!
    @IBOutlet var imgSix: UIImageView!
    @IBOutlet var imgSeven: UIImageView!
    @IBOutlet var CollectionViewOutlet: UICollectionView!
    
    var nextDate = Date()
    var lastdate : String?
    var userModel :Instructor?
    var currentDate : String?
    var date = Date()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let date = DateFormatter()
        date.dateFormat = "yyyy-MM-dd"
        currentDate = date.string(from: Date())
        let todayDate = Date()
        let Datenext = DateFormatter()
        Datenext.dateFormat = "yyyy-MM-dd"
        nextDate = Datenext.calendar.date(byAdding: .day, value: 6,to: todayDate)!
        lastdate = Datenext.string(from:nextDate)
       // Alamofire.request("http://34.195.206.185/api/instructor-home")
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        fetchData()
    }
    func weekDays(substring:String)->String?
    {
        let str = substring
        let endIndex = str.index(str.startIndex, offsetBy:3)
        print(str.substring(to: endIndex))
        return str.substring(to: endIndex)
    }
    func fetchData() {
        
        let param:[String:Any] = ["access_token" : "ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8" , "page_no": "1" ,"page_size" : "7" ,"date_selected" :"\(date)"]
        
            ApiHandler.fetchData(urlStr: "instructor-home", parameters: param) {(jsonData) in
            self.userModel = Mapper<Instructor>().map(JSONObject: jsonData)
            self.CollectionViewOutlet.reloadData()
            print(self.userModel?.msg ?? "")
            print(jsonData!)
            print(self.userModel?.data?[0].date ?? "")
            self.lblDate.text = self.userModel?.data?[0].date ?? ""
            print(self.userModel?.data?[0].date1 ?? "")
                print(self.userModel?.data?[0].day ?? "")
                self.dayOne.text = self.weekDays(substring:(self.userModel?.data?[0].day!)!)
                self.dayTwo.text = self.weekDays(substring:(self.userModel?.data?[1].day!)!)
                self.dayThree.text = self.weekDays(substring:(self.userModel?.data?[2].day!)!)
                self.dayFour.text = self.weekDays(substring:(self.userModel?.data?[3].day!)!)
                self.dayFive.text = self.weekDays(substring:(self.userModel?.data?[4].day!)!)
                self.daySix.text = self.weekDays(substring:(self.userModel?.data?[5].day!)!)
                self.daySeven.text = self.weekDays(substring:(self.userModel?.data?[6].day!)!)
                SVProgressHUD.dismiss()
        }
    }
 /*  func weekDays(substring:String)->String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: substring) {
            let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let components = calendar.components(.weekday, from: todayDate)
            let weekOfDay = components.weekday
            switch (weekOfDay!) {
            case 1:
                return "Sun"
            case 2:
                return "Mon"
            case 3:
                return "Tue"
            case 4:
                return "Wed"
            case 5:
                return "Thur"
            case 6:
                return "Fri"
            case 7:
                return "Sat"
            default:
                return ""
            }
        }
        return ""
    }*/
    func image(imageType:UIImageView)
    {
        let arrayOfImages = [imgOne , imgTwo ,imgThree ,imgFour ,imgFive , imgSix ,imgSeven]
        for index in 0..<arrayOfImages.count
        {
            if(imageType == arrayOfImages[index])
            {
                imageType.image = UIImage(named: "screenShot2")
            }else{
                arrayOfImages[index]?.image = nil
            }
        }
    }
    func labelfonts(labelsForDay:UILabel)
    {
        var arrayOfDays = [ dayOne , dayTwo ,dayThree ,dayFour ,dayFive ,daySix, daySeven]
        for index in 0..<arrayOfDays.count
        {
            if(labelsForDay == arrayOfDays[index])
            {
                labelsForDay.font = UIFont.systemFont(ofSize: 14, weight: 2)
            }
            else
            {
                arrayOfDays[index]?.font = UIFont.systemFont(ofSize:12, weight:0)
            }
        }
    }
    /*func labelColor(colorlabels:UIColor)
    {
        var arrayOfLabelColor = [dayOne , dayTwo , dayThree , dayFour , dayFive , daySix , daySeven ]
        for index in 0..<arrayOfLabelColor.count
        {
            if(colorlabels == arrayOfLabelColor[index])
            {
               colorlabels.cgColor = UIColor.black
            }
            
        }
    }*/
    }
extension ViewController :  UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
   
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if(indexPath.item % 7 == 0)
        {
            self.image(imageType: imgOne)
            self.labelfonts(labelsForDay: dayOne)
            lblDate.text = self.userModel?.data?[indexPath.item].date ?? ""
        }
        else if (indexPath.item % 7 == 1)
        {
             self.image(imageType: imgTwo)
            self.labelfonts(labelsForDay: dayTwo)
             lblDate.text = self.userModel?.data?[indexPath.item].date ?? ""
        }
        else if (indexPath.item % 7 == 2)
        {
            self.image(imageType: imgThree)
            self.labelfonts(labelsForDay: dayThree)
             lblDate.text = self.userModel?.data?[indexPath.item].date ?? ""
        }
        else if (indexPath.item % 7 == 3)
        {
            self.image(imageType: imgFour)
            self.labelfonts(labelsForDay: dayFour)
             lblDate.text = self.userModel?.data?[indexPath.item].date ?? ""
        }
        else if (indexPath.item % 7 == 4)
        {
            self.image(imageType: imgFive)
            self.labelfonts(labelsForDay: dayFive)
             lblDate.text = self.userModel?.data?[indexPath.item].date ?? ""
        }
        else if (indexPath.item % 7 == 5)
        {
            self.image(imageType: imgSix)
            self.labelfonts(labelsForDay: daySix)
             lblDate.text = self.userModel?.data?[indexPath.item].date ?? ""
        }
        else if (indexPath.item % 7 == 6)
        {
            self.image(imageType: imgSeven)
            self.labelfonts(labelsForDay: daySeven)
             lblDate.text = self.userModel?.data?[indexPath.item].date ?? ""
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (self.userModel?.data?.count) ?? 0
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:CollectionView = (collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionIdentifier", for: indexPath) as?CollectionView)!
        cell.details = (userModel?.data?[indexPath.row].details)!
        return cell
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        var visibleRect = CGRect()
        visibleRect.origin = CollectionViewOutlet.contentOffset
        visibleRect.size = CollectionViewOutlet.bounds.size
        
        //indexPathsForVisibleItems might work for most situations, but sometimes it returns an array with more than one index path and it can be tricky figuring out the one you want.
        //CGRect visibleRect = (CGRect){.origin = self.collectionView.contentOffset, .size = self.collectionView.bounds.size};
        //CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
       // IndexPath *visibleIndexPath = [self.collectionViewindexPathForItemAtPoint:visiblePoint];
       /* func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            var visibleRect = CGRect()
            
            visibleRect.origin = collectionView.contentOffset
            visibleRect.size = collectionView.bounds.size
            
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            
            let visibleIndexPath: IndexPath = collectionView.indexPathForItem(at: visiblePoint)!
            
            print(visibleIndexPath)
        }*/
        
        let visibleCells = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath : IndexPath = CollectionViewOutlet.indexPathForItem(at: visibleCells)!
        print(visibleIndexPath)
        
        //The method [collectionView visibleCells] give you all visibleCells array you want.
        
        let endData = (userModel?.data?.count)! - 1
        if visibleIndexPath.row == endData
        {
           // Alamofire.request("http://34.195.206.185/api/instructor-home")
            let param: [String:Any] = ["access_token": "ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8", "page_no": "2", "page_size": "7","last_date": lastdate ?? "", "date_selected": currentDate!]
            ApiHandler.fetchData(urlStr: "instructor-home", parameters: param)
            {(jsonData) in
                let userModelAppend = Mapper<Instructor>().map(JSONObject: jsonData)
                for temp in 0..<7
                {
                    self.userModel?.data?.append((userModelAppend?.data?[temp])!)
                }
                self.CollectionViewOutlet.reloadData()
            }
        }
}
}













