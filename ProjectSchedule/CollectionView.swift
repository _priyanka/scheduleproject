//
//  CollectionView.swift
//  ProjectSchedule
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class CollectionView: UICollectionViewCell {
    @IBOutlet var TableviewOutlet: UITableView!
    var details : [Details]?
    var subjects : [Subjects]?
    var locations : [Locations]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        TableviewOutlet.delegate = self
       TableviewOutlet.dataSource = self
    }
    
}


extension CollectionView: UITableViewDelegate,UITableViewDataSource  {
    
    

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
            return (details?.count)!

    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
            
            let cell:TableViewCell = tableView.dequeueReusableCell(withIdentifier: "TableIdentifier", for: indexPath) as!TableViewCell
        
        if indexPath.row == 0
        {
            cell.viewTwoOutlet.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        }
        else{
            cell.viewTwoOutlet.backgroundColor = UIColor(red: 184/255, green: 184/255, blue: 184/255, alpha: 1)
        }
        if indexPath.row == (details?.count)!-1
        {
            cell.view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        }
        else{
            cell.view.backgroundColor = UIColor(red: 184/255, green: 184/255, blue: 184/255, alpha: 1)
        }
        /*if indexPath.row < (details?.count)!
        {
            cell.TableviewOutlet = (details?[indexPath.row])!
        }*/
        
        
        
          //cell.objectTwo = moreArray[indexPath.row]
       cell.timeLabel.text = details?[indexPath.row].start_time1
        for subjects in(details?[indexPath.row].subjects)!
        {
       cell.subjectLabel.text = subjects.subject_name ?? ""
        }
        cell.perStudentLabel.text = details?[indexPath.row].charge_student
       cell.enrolledLabel.text = "\(details?[indexPath.row].enrolled ?? 0)"
        cell.ageLabel.text = details?[indexPath.row].age_group
        cell.hourLabel.text = details?[indexPath.row].time_duration
        cell.addressLabel.text = "Home "+(details?[indexPath.row].day_location!)!
       
        /*cell.object = (details?[indexPath.row])!*/
        
        
        
        return cell
}
}
