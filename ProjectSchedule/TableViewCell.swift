//
//  TableViewCell.swift
//  ProjectSchedule
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var view: UIView!
    @IBOutlet var viewTwoOutlet: UIView!
    @IBOutlet var subjectLabel: UILabel!
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var enrolledLabel: UILabel!
    @IBOutlet var ageLabel: UILabel!
    @IBOutlet var perStudentLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var viewOutlet: UIView!
    @IBOutlet var viewImageOutlet: UIImageView!
    
    /*var object : Details?
    {
        didSet{
            updateUI()
        }
    }*/
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewOutlet.layer.shadowColor = UIColor.gray.cgColor
        viewOutlet.layer.shadowOpacity = 1
        viewOutlet.layer.shadowOffset = CGSize.zero
        viewOutlet.layer.shadowRadius = 0.5
        viewImageOutlet.layer.cornerRadius = 9
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
